/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */

#include "controls.h"

alt_up_ps2_dev * init_keyboard(alt_up_ps2_dev *keyboard_device){

	keyboard_device=alt_up_ps2_open_dev("/dev/ps2_0");
	//print("alt_UP\n");

	if(keyboard_device==NULL)
		printf("Failed to initialize keyboard!\n");
	else{
		alt_up_ps2_init(keyboard_device);
		//print("Keyboard initialized!\n");
		alt_up_ps2_clear_fifo(keyboard_device);
	}

	return keyboard_device;
}
