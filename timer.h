/*
 * timer.h
 *
 *  Created on: 2014-01-28
 *      Author: Henry
 */

#ifndef TIMER_H_
#define TIMER_H_

void sys_clock_timer(void);
void clk_ticks(int duration3);
void timestamp(int duration);
void hard_timer(int duration2);



#endif /* TIMER_H_ */
