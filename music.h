/*
 * music.h
 *
 *  Created on: 2014-01-23
 *      Author: 1st//
 */

// GUYS CAN WE PLEASE USE GOOD COMMENTS?

#ifndef MUSIC_H_
#define MUSIC_H_

#define HEADER_SIZE 44
#define SAMPLE_SIZE 95		// # bytes to play in 1 interrupt iteration
//#define SEGMENT_SIZE 768	// # bytes to load in 1 load_music()
#define FILE_SIZE 986548
#define JUMP_SIZE 30832
#define SLIDE_SIZE 50878
#define HIT_SIZE 9088

#include <system.h>
#include <stdio.h>
#include <stdlib.h>
#include <io.h>
#include <assert.h>
#include <altera_up_avalon_audio.h>
#include <altera_up_avalon_audio_and_video_config.h>    //audio hardware
#include <altera_up_avalon_audio_and_video_config_regs.h>
#include <altera_up_avalon_audio_regs.h>
#include <altera_up_avalon_audio_and_video_config_regs.h>
#include <sys/alt_irq.h>
#include "sd_card.h"

extern unsigned int sound_data [FILE_SIZE];
extern unsigned int* sound_buff;
extern unsigned int byte_data[2];
extern unsigned int sound_data_counter;
extern unsigned int end_file_counter;
extern int header_bit;
extern short int music_handle;
extern unsigned int end_of_segment;
extern unsigned int end_of_file;
extern unsigned int jump_sound [JUMP_SIZE];
extern unsigned int slide_sound [SLIDE_SIZE];
extern unsigned int hit_sound [HIT_SIZE];

extern jump_flag;
extern slide_flag;
extern finishedPlaying;
extern unsigned int jump_counter;
extern unsigned int slide_counter;
extern unsigned int hit_counter;
alt_up_audio_dev *  av_config_setup();
	//@effects: Initialize audio device

void audio_irq_init(alt_up_audio_dev *audio_dev);
	//@effects: initialize irq (interrupt request) for audio write

void load_music_header(void);
	//@effects: load the wav file header to music buffer

void load_music_body(void);
	//@effects: load the wav file to music buffer

void close_music(void);
	//@effects: close music file

void music_isr(void* context, alt_u32 id);
	//@effects:	interrupt service routine to play audio

void play_music(alt_up_audio_dev *audio_dev);
	//@effects: call this function to play music

void load_jump_body(void);
	//@effects: load the jump wav file to sound buffer

void load_slide_body(void);
	//@effects: load the slide wav file to sound buffer

void load_hit_body(void);
	//@effects: load the slide wav file to sound buffer

void jump_isr(void* context, alt_u32 id);

void slide_isr(void* context, alt_u32 id);

void hit_isr(void* context, alt_u32 id);

void play_jump_sound(void);
	//@effects: call this function to play jump sound without interrupt

void play_slide_sound(void);
	//@effects: call this function to play slide sound without interrupt

void play_hit_sound(void);
	//@effects: call this function to play hit sound without interrupt

void load_sound(void);

void sound_isr(void* context, alt_u32 id);

void play_sound(alt_up_audio_dev *audio_dev);


#endif /* MUSIC_H_ */
