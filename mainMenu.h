#ifndef MAINMENU_H_
#define MAINMENU_H_

#include "gamelogic.h"
#include <altera_up_avalon_ps2.h>		//ps2 keyboard
#include <altera_up_ps2_keyboard.h>		//ps2 keyboard (need both)
#include <altera_up_avalon_video_pixel_buffer_dma.h>

extern int screen;
extern char ascii_keyboard;
extern unsigned char displayed_score[10];

void mainMenu(alt_up_pixel_buffer_dma_dev *pixel_buffer);
void deathScreen(alt_up_pixel_buffer_dma_dev *pixel_buffer);



#endif
