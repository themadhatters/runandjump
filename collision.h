/*
 * collision.h
 *
 *  Created on: 2014-02-04
 *      Author: 1st
 */

#ifndef COLLISION_H_
#define COLLISION_H_

#include "box_sys.h"

extern int last_box_x;
extern int last_box_lane;
extern box boxes[];
extern int screen;

void detectCollision(alt_up_pixel_buffer_dma_dev *pixel_buffer);

void draw_collision_box(alt_up_pixel_buffer_dma_dev *pixel_buffer);

#endif /* COLLISION_H_ */
