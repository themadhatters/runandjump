/*
 * gamelogic.h
 *
 *  Created on: 2014-02-04
 *      Author: 1st
 */

#ifndef GAMELOGIC_H_
#define GAMELOGIC_H_

#include "player_box.h"
#define RUNNING 0
#define JUMPING 1
#define SLIDING 2
#define UP 0x57
#define DOWN 0x53
#define ENTER 0x44

int actionLocked;
player_Box collision_box;
extern int status;
extern int jump_flag;
extern int drop_flag;
extern int slide_flag;
extern int boxX[4];
extern int boxY[4];
extern int stickman_position;	//A counter to change the stickman based on the keyboard input
extern int count_slide;
extern finishedPlaying;

void doAction(char input);

void run(void);

void jump(void);

void slide(void);

void updateStatus(char input);

void lockAction(void);

void unlockAction(void);

int checkCollision(void);

void check_jump(int stickman_speed);
	//@effects:

void check_drop(void);
	//@effects:

void check_slide(int stickman_speed);
	//@effects:




#endif /* GAMELOGIC_H_ */
