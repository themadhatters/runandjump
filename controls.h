/*
 * controls.h
 *
 *  Created on: 2014-01-23
 *      Author: 1st
 */

#ifndef CONTROLS_H_
#define CONTROLS_H_
#include "altera_up_avalon_ps2.h"
#include "altera_up_ps2_keyboard.h"

alt_up_ps2_dev * init_keyboard(alt_up_ps2_dev *keyboard_device);

#endif /* CONTROLS_H_ */
