/*
 * player_box.c
 *
 *  Created on: 2014-02-01
 *      Author: 1st
 */

#include "player_box.h"

void get_player_Box_coords_x(player_Box collision_box, int *player_BoxX){
	//@Effects:	Copy collision_box.TL_x, collision_box.TR_x, collision_box.BL_x, collision_box.BR_x to an int array.
	// 			Returns an array that contains all 4 variables in the same order.

	player_BoxX[0] = collision_box.TL_x;
	player_BoxX[1] = collision_box.TR_x;
	player_BoxX[2] = collision_box.BL_x;
	player_BoxX[3] = collision_box.BR_x;
}

void get_player_Box_coords_y(player_Box collision_box, int *player_BoxY){
	//@Effects: Copy collision_box.TL_y, collision_box.TR_y, collision_box.BL_y, collision_box.BR_y to an int array.
	// 			Returns an array that contains all 4 variables in the same order.

	player_BoxY[0] = collision_box.TL_y;
	player_BoxY[1] = collision_box.TR_y;
	player_BoxY[2] = collision_box.BL_y;
	player_BoxY[3] = collision_box.BR_y;
}

void update_sliding_player_Box_coords(player_Box *collision_box){
	//@Effects: Change the coordinates of all 4 points of the player_Box when sliding flag is active.

		collision_box->TL_x =  20;
		collision_box->TR_x =  100;
		collision_box->BL_x =  20;
		collision_box->BR_x =  100;
		collision_box->TL_y =  150;
		collision_box->TR_y =  150;
		collision_box->BL_y =  200;
		collision_box->BR_y =  200;
}

void update_jumping_player_Box_coords(player_Box *collision_box,int stickman_speed){
	//@Effects: Change the coordinates of all 4 points of the player_Box when jump_flag = 1 && drop_flag == 0.
	collision_box->TL_y = collision_box->TL_y -stickman_speed;
	collision_box->TR_y = collision_box->TR_y -stickman_speed;
	collision_box->BL_y = collision_box->BL_y -stickman_speed;
	collision_box->BR_y = collision_box->BR_y -stickman_speed;
}

void update_dropping_player_Box_coords(player_Box *collision_box, int stickman_speed){
	//@Effects: Change the coordinates of all 4 points of the player_Box when jump_flag = 1 && drop_flag == 1.
	collision_box->TL_y = collision_box->TL_y +stickman_speed;
	collision_box->TR_y = collision_box->TR_y +stickman_speed;
	collision_box->BL_y = collision_box->BL_y +stickman_speed;
	collision_box->BR_y = collision_box->BR_y +stickman_speed;
}

void update_resetting_player_Box_coords(player_Box *collision_box){
	//@Effects: Change the coordinates of all 4 points of the player_Box when jump_flag = 0 && drop_flag == 0.

	collision_box->TL_x = 30;
	collision_box->TR_x = 60;
	collision_box->BL_x = 30;
	collision_box->BR_x = 60;
	collision_box->TL_y = 100;
	collision_box->TR_y = 100;
	collision_box->BL_y = 190;
	collision_box->BR_y = 190;
}
