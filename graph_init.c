
#include "graph_init.h"

alt_up_pixel_buffer_dma_dev* graph_init(alt_up_pixel_buffer_dma_dev* pixel_buffer) {
	//@effects: Initialize the dma pixel buffer to display graphics
	
	pixel_buffer = alt_up_pixel_buffer_dma_open_dev("/dev/pixel_buffer_dma");
	//EFFECTS: Use the name of your pixel buffer DMA core

	alt_up_pixel_buffer_dma_change_back_buffer_address(pixel_buffer, pixel_buffer_addr1);
	//EFFECTS: Set the 1st buffer address

	//print("Swap background and foreground buffers.\n");
	alt_up_pixel_buffer_dma_swap_buffers(pixel_buffer);
	//EFFECTS:	Swap background and foreground buffers
	while (alt_up_pixel_buffer_dma_check_swap_buffers_status(pixel_buffer));
	//EFFECTS: 	Wait for the swap to complete

	//print("Set the 2nd buffer address.\n");
	alt_up_pixel_buffer_dma_change_back_buffer_address(pixel_buffer, pixel_buffer_addr2);
	//EFFECTS: Set the 2nd buffer address

	//print("Clearing buffer 0 and 1.\n");
	alt_up_pixel_buffer_dma_clear_screen(pixel_buffer, 0);
	//EFFECTS: Clear foreground buffer (0); this makes all pixels black
	alt_up_pixel_buffer_dma_clear_screen(pixel_buffer, 1);
	//EFFECTS: Clear background buffer (1); this makes all pixels black

	return pixel_buffer;
}
