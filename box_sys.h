/*
 * box_sys.h
 *
 *  Created on: 2014-01-29
 *      Author: Ryan Yap
 */

#ifndef BOX_SYS_H_
#define BOX_SYS_H_

//#include "box_sys.c"
#define LENGTH_OF_BOX 40
//TO-DO:-----------------------------------------------------------------------------------
//PLEASE SPECIFY THE COLOUR OF THE BOX
#define COLOUR_OF_BOX 0xffe3
//-----------------------------------------------------------------------------------------

extern	int x_coordinate_of_boxes[5];
extern	int boxes_lanes[5];


typedef struct box_{
	int x;
	int y;
	int colour;
	int lane;
	int exist;
}box;

int get_last_box_coordinate( box *boxes, int size);
void get_all_four_y_coordinates(int boxY[], int size, int last_lane);
void get_all_four_x_coordinates(int boxX[],int size,int last_coordinate);
int get_last_box_lane( box *boxes, int size);

int get_first_box_coordinate( box *boxes, int size);
//@requires: an array of structs with size == 5
//@modifies: boxes[5]'s variable.
//@effects: Return the coordinate of the box closest to the right edge of the screen.

void get_lanes(box* boxes,int lanes[],int b_size,int l_size );

//void init_boxes(box boxes[], int size);
void init_boxes(box *boxes, int size);
	//@requires: an array of structs with size == 5
	//@modify: boxes[5]'s variable.
	//@affect: For every box, initializes the variables containing in the struct to 0.(to remove unwanted garbage value
	//         when the boxes are created.)

void create_new_box(box *boxes, int size);
	//@modifies: boxes[].exist
	//@effects:  Find a box that doesn't exist on the screen and change it's exist bit from 0 to 1.

void get_coordinates_of_all_boxes(box* boxes, int coordinates[],int b_size,int c_size );
	//@Effects: Copy boxes[i].x to an int array. Returns an array that consists of the x-coordinate of each box. If a box doesn't exist on the screen,
	//         it's corresponding slot in the int array get -1.

void move_all_boxes_to_the_left(box* boxes, int size,int box_speed);
	//@Effects: Move all existing boxes to the left by one pixel.
	//			Calls move_box_to_the_left() for all boxes.

void remove_box(box* boxes, int size, int index);
	//@Modifies: boxes[index].exist
	//@Effects: set the exist bit of box[index] to 0;

void move_box_to_the_left(box* boxes, int size, int index,int box_speed);
	//@Requires: boxes[index].x<=320;
	//@Modifies: boxes[index].x
	//@Effects: moves the coordinate of box[index] to the left by one pixel. If boxes[index].x==0, remove the box.

int check_if_boxes_exist(box* boxes, int size);
	//@Modifies: nothing
	//@Effects: Check if there is boxes existing on the screen. If no box exists, return 0. Returns 1 if at least 1 box exists.

int generate_random_number(void);
	//@effects: outputs a random number from 0 to 10

void delay(void);
	//@effects: a manual delay

void draw_boxes(alt_up_pixel_buffer_dma_dev* pixel_buffer);
	//@effects: draw all boxes on background buffer

#endif /* BOX_SYS_H_ */
