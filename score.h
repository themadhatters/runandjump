/*
 * int to char converter
 * Source:
 * http://www.milouchev.com/blog/2013/03/interview-questions-itoa-implementation-in-c/
 *
 */
#ifndef SCORE_H_
#define SCORE_H_

int itoa(int n, char* out);
void reverse(char* str, int length);

#endif /* BOX_SYS_H_ */
