
/*
 * background.h
 *
 *  Created on: 2014-01-23
 *      Author: 1st
 */
#ifndef BACKGROUND_H_
#define BACKGROUND_H_

#include <altera_up_avalon_video_pixel_buffer_dma.h>

void drawBackground(alt_up_pixel_buffer_dma_dev *pixel_buffer);

#endif /* BACKGROUND_H_ */
