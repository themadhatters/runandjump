/*
 * music.c
 *
 *  Created on: 2014-01-23
 *      Author: 1st//
 */

#include "music.h"


alt_up_audio_dev *  av_config_setup() {
	//@effects: Initialize audio device

	alt_up_av_config_dev* audio_config;

	audio_config = alt_up_av_config_open_dev("/dev/audio_and_video_config_0");
	if (audio_config == NULL)
		printf("ERROR in opening audio config\n");
	else
		printf("Opened audo config\n");

	while (!alt_up_av_config_read_ready(audio_config)) {
	}

	// open the Audio port
	alt_up_audio_dev * audio_dev = alt_up_audio_open_dev("/dev/audio_0");
	if (audio_dev == NULL)
		printf("Error: could not open audio device \n");
	else
		printf("Opened audio device \n");

	return audio_dev;
}

void audio_irq_init(alt_up_audio_dev *audio_dev){
	//@effects: initialize irq (interrupt request) for audio write

	alt_up_audio_disable_read_interrupt(audio_dev);
	alt_up_audio_disable_write_interrupt(audio_dev);
	printf("Audio IRQ Initialized...\n");
}

void load_music_header(void){
	//@effects: load the wav file header to music buffer

	music_handle = alt_up_sd_card_fopen("music.wav", 0);
	printf("music_handle: %i \n", music_handle );

	if (header_bit == 0){
		// skip header
		int i;
		for (i = 0; i < HEADER_SIZE; i++)
		{
			alt_up_sd_card_read(music_handle);
		}
		header_bit = 1;
	}
}

//void load_music_body(void){
//	//@effects: load the wav file to music buffer
//
//	printf("music_handle: %i \n", music_handle );
//
//	// read and store sound data into memory
//	int i;
//	for (i = 0; i < SEGMENT_SIZE; i++)
//	{
//		music_data[i] = alt_up_sd_card_read(music_handle);
//	}
//	sound_buff = (unsigned int*) malloc(SAMPLE_SIZE * sizeof(unsigned int));
//}

void close_music(void){
	//@effects: close music file

	alt_up_sd_card_fclose(music_handle);
}

//void music_isr(void* context, alt_u32 id){
//	//@effects:	interrupt service routine to play audio
//
//	// FIFO is 75% empty, need to fill it up
//	int i;
//	for (i = 0; i < SAMPLE_SIZE; i++)
//	{
//		// take 2 bytes at a time
//		byte_data[0] = music_data[sound_data_counter];
//		byte_data[1] = music_data[sound_data_counter+1];
//		sound_data_counter += 2;
//		end_file_counter += 2;
//
//		// combine the two bytes and store into sample buffer
//		sound_buff[i] = (byte_data[1] << 8) | byte_data[0];
//
//		// if we reach end of segment, signal main.c to read next sample
//		if (sound_data_counter >= SEGMENT_SIZE){
//			sound_data_counter = 0;
//			end_of_segment = 1;
//		}
//
//		// if we reach end of file, then we loop back to start over
//		if (end_file_counter >= FILE_SIZE){
//			end_file_counter = 0;
//			end_of_file = 1;
//		}
//	}
//	// finally, we write this sample data to the FIFO
//	alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE, ALT_UP_AUDIO_LEFT);
//	alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE, ALT_UP_AUDIO_RIGHT);
//}

//void play_music(alt_up_audio_dev *audio_dev){
//	//@effects: call this function to play music
//
//	// register isr
//	sound_data_counter = 0;
//	alt_irq_register(AUDIO_0_IRQ, audio_dev, music_isr);
//
//	// enable interrupt
//	alt_irq_enable(AUDIO_0_IRQ);
//	alt_up_audio_enable_write_interrupt(audio_dev);
//}

void load_jump_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("jump.wav", 0);
	printf("file_handle: %i \n", file_handle);

	int i;
	for (i = 0; i < HEADER_SIZE; i++){
		alt_up_sd_card_read(file_handle);
	}

	int index = 0;
	while (1)
	{
		jump_sound [index] = alt_up_sd_card_read(file_handle);
		if(jump_sound [index] == -1)
			break;
		index++;
	}
	alt_up_sd_card_fclose(file_handle);
}

void load_slide_body(void){
	//@effects: load the slide wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("slide.wav", 0);
	printf("file_handle: %i \n", file_handle);

	int i;
	for (i = 0; i < HEADER_SIZE; i++){
		alt_up_sd_card_read(file_handle);
	}

	int index = 0;
	while (1)
	{
		slide_sound [index] = alt_up_sd_card_read(file_handle);
		if(slide_sound [index] == -1)
			break;
		index++;
	}
	alt_up_sd_card_fclose(file_handle);
}

void load_hit_body(void){
	//@effects: load the slide wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("hit.wav", 0);
	printf("file_handle: %i \n", file_handle);

	int i;
	for (i = 0; i < HEADER_SIZE; i++){
		alt_up_sd_card_read(file_handle);
	}

	int index = 0;
	while (1)
	{
		hit_sound [index] = alt_up_sd_card_read(file_handle);
		if(hit_sound [index] == -1)
			break;
		index++;
	}
	alt_up_sd_card_fclose(file_handle);
}


void jump_isr(void* context, alt_u32 id){
	int i;
	for (i = 0; i < SAMPLE_SIZE; i++)
	{
		// take 2 bytes at a time
		byte_data[0] = jump_sound[sound_data_counter];
		byte_data[1] = jump_sound[sound_data_counter+1];
		sound_data_counter += 2;
		//end_file_counter += 2;

		// combine the two bytes and store into sample buffer
		sound_buff[i] = (byte_data[1] << 8) | byte_data[0];

		// if we reach end of segment, signal main.c to read next sample
		if (sound_data_counter >= FILE_SIZE){
			sound_data_counter = 0;
		//	end_of_segment = 1;
		}
		// finally, we write this sample data to the FIFO
		alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE, ALT_UP_AUDIO_LEFT);
		alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE, ALT_UP_AUDIO_RIGHT);
	}
}

void slide_isr(void* context, alt_u32 id){
	int i;
	for (i = 0; i < SAMPLE_SIZE; i++)
	{
		// take 2 bytes at a time
		byte_data[0] = slide_sound[sound_data_counter];
		byte_data[1] = slide_sound[sound_data_counter+1];
		sound_data_counter += 2;
		//end_file_counter += 2;

		// combine the two bytes and store into sample buffer
		sound_buff[i] = (byte_data[1] << 8) | byte_data[0];

		// if we reach end of segment, signal main.c to read next sample
		if (sound_data_counter >= FILE_SIZE){
			sound_data_counter = 0;
		//	end_of_segment = 1;
		}
		// finally, we write this sample data to the FIFO
		alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE, ALT_UP_AUDIO_LEFT);
		alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE, ALT_UP_AUDIO_RIGHT);
	}
}

void hit_isr(void* context, alt_u32 id){
	int i;
	for (i = 0; i < SAMPLE_SIZE; i++)
	{

		// take 2 bytes at a time
		byte_data[0] = hit_sound[sound_data_counter];
		byte_data[1] = hit_sound[sound_data_counter+1];
		sound_data_counter += 2;
		//end_file_counter += 2;

		// combine the two bytes and store into sample buffer
		sound_buff[i] = (byte_data[1] << 8) | byte_data[0];

		// if we reach end of segment, signal main.c to read next sample
		if (sound_data_counter >= FILE_SIZE){
			sound_data_counter = 0;
		//	end_of_segment = 1;
		}
		// finally, we write this sample data to the FIFO
		alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE, ALT_UP_AUDIO_LEFT);
		alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE, ALT_UP_AUDIO_RIGHT);
	}
}

//void play_jump_sound(void){
//	//@effects: call this function to play jump sound without interrupt
//
//	// register isr
//	sound_data_counter = 0;
//	alt_irq_register(AUDIO_0_IRQ, audio_dev, jump_isr);
//
//	// enable interrupt
//	alt_irq_enable(AUDIO_0_IRQ);
//	alt_up_audio_enable_write_interrupt(audio_dev);
//}

//void play_slide_sound(void){
//	//@effects: call this function to play music without interrupt
//
//	// register isr
//	sound_data_counter = 0;
//	alt_irq_register(AUDIO_0_IRQ, audio_dev, slide_isr);
//
//	// enable interrupt
//	alt_irq_enable(AUDIO_0_IRQ);
//	alt_up_audio_enable_write_interrupt(audio_dev);
//}
//
//void play_hit_sound(void){
//	//@effects: call this function to play music without interrupt
//
//	// register isr
//	sound_data_counter = 0;
//	alt_irq_register(AUDIO_0_IRQ, audio_dev, hit_isr);
//
//	// enable interrupt
//	alt_irq_enable(AUDIO_0_IRQ);
//	alt_up_audio_enable_write_interrupt(audio_dev);
//}

void load_sound(void){
	//@effects:

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("music.wav", 0);

	// skip header
	int i;
	for (i = 0; i < HEADER_SIZE; i++)
	{
		alt_up_sd_card_read(file_handle);
	}

	// read and store sound data into memory
	for (i = 0; i < FILE_SIZE; i++)
	{
		sound_data[i] = alt_up_sd_card_read(file_handle);
	}
	alt_up_sd_card_fclose(file_handle);
}

void sound_isr(void* context, alt_u32 id){
	//@effects:	interrupt service routine to play audio

//	if (!finishedPlaying) {
//		int j;
//		for (j = 0; j < SAMPLE_SIZE/2; j++)
//		{
//			if (jump_flag){
//				byte_data[0] = jump_sound[jump_counter];
//				byte_data[1] = jump_sound[jump_counter+1];
//				jump_counter += 2;
//				if (jump_counter >= JUMP_SIZE) {
//					finishedPlaying = 1;
//					jump_counter = 0;
//				}
//			} else if (slide_flag) {
//				byte_data[0] = slide_sound[slide_counter];
//				byte_data[1] = slide_sound[slide_counter+1];
//				slide_counter += 2;
//				if (slide_counter >= SLIDE_SIZE) {
//					finishedPlaying = 1;
//					slide_counter = 0;
//				}
//			}  else {
//				byte_data[0] = hit_sound[hit_counter];
//				byte_data[1] = hit_sound[hit_counter+1];
//				hit_counter += 2;
//				if (hit_counter >= HIT_SIZE) {
//					finishedPlaying = 1;
//					hit_counter = 0;
//				}
//			}
//			// combine the two bytes and store into sample buffer
//			sound_buff[j] = (byte_data[1] << 8) | byte_data[0];
//		}
//		// finally, we write this sample data to the FIFO
//		alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE/2, ALT_UP_AUDIO_LEFT);
//		alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE/2, ALT_UP_AUDIO_RIGHT);
//	}

	// FIFO is 75% empty, need to fill it up

	int i;
	for (i = 0; i < SAMPLE_SIZE; i++)
	{
		if (jump_flag && !finishedPlaying) {
			byte_data[0] = jump_sound[jump_counter];
			byte_data[1] = jump_sound[jump_counter+1];
			jump_counter += 2;
			if (jump_counter >= JUMP_SIZE) {
				finishedPlaying = 1;
				jump_counter = 0;
			}
		} else if (slide_flag && !finishedPlaying) {
			byte_data[0] = slide_sound[slide_counter];
			byte_data[1] = slide_sound[slide_counter+1];
			slide_counter += 2;
			if (slide_counter >= SLIDE_SIZE) {
				finishedPlaying = 1;
				slide_counter = 0;
			}

//		}  else if (&& !finishedPlaying){
//			byte_data[0] = hit_sound[hit_counter];
//			byte_data[1] = hit_sound[hit_counter+1];
//			hit_counter += 2;
//			if (hit_counter >= HIT_SIZE) {
//				finishedPlaying = 1;
//				hit_counter = 0;
//			}

		} else {
			// take 2 bytes at a time
			byte_data[0] = sound_data[sound_data_counter];
			byte_data[1] = sound_data[sound_data_counter+1];

			//end_file_counter += 2;
		}
		sound_data_counter += 2;

		// combine the two bytes and store into sample buffer
		sound_buff[i] = (byte_data[1] << 8) | byte_data[0];

		// if we reach end of segment, signal main.c to read next sample
		if (sound_data_counter >= FILE_SIZE){
			sound_data_counter = 0;
		//	end_of_segment = 1;
		}

		// if we reach end of file, then we loop back to start over
//		if (end_file_counter >= FILE_SIZE){
//			end_file_counter = 0;
//			end_of_file = 1;
//		}
	}
	// finally, we write this sample data to the FIFO
	alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE, ALT_UP_AUDIO_LEFT);
	alt_up_audio_write_fifo(context, sound_buff, SAMPLE_SIZE, ALT_UP_AUDIO_RIGHT);
}

void play_sound(alt_up_audio_dev *audio_dev){
	//@effects: call this function to play music

	// register isr
	sound_data_counter = 0;
	alt_irq_register(AUDIO_0_IRQ, audio_dev, sound_isr);

	// enable interrupt
	alt_irq_enable(AUDIO_0_IRQ);
	alt_up_audio_enable_write_interrupt(audio_dev);
}
