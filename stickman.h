/*
 * stickman.h
 *
 *  Created on: 2014-01-23
 *      Author: Hao
 *
 *  This file contains all functions and constants needed to draw the stickman.
 *
 */

/*
#define head_top_left_x		40;
#define head_top_left_y		80;
#define head_top_right_x	50;
#define head_top_right_y	80;
#define head_bot_left_x		;
#define head_bot_left_y	;
#define head_bot_right_x	;
#define head_bot_right_y	;
#define shoulder_x			;
#define shoulder_y			;
#define elbow_left_x		;
#define elbow_left_y		;
#define elbow_right_x		;
#define elbow_right_y		;
#define hand_left_x			;
#define hand_left_y			;
#define hand_right_x		;
#define hand_right_y		;
#define hips_x				;
#define hips_y				;
#define knee_left_x			;
#define knee_left_y			;
#define knee_right_x		;
#define knee_right_y		;
#define foot_left_x			;
#define foot_left_y			;
#define foot_right_x		;
#define foot_right_y		;
*/

void draw_Stickman_run_1(alt_up_pixel_buffer_dma_dev *pixel_buffer, int stickman_position);
	//@effect: draw 1st frame of running stickman

void draw_Stickman_run_2(alt_up_pixel_buffer_dma_dev *pixel_buffer, int stickman_position);
	//@effect: draw 2nd frame of running stickman

//void draw_Stickman_jump(alt_up_pixel_buffer_dma_dev *pixel_buffer, int stickman_position);	//unused

void draw_Stickman_slide_1(alt_up_pixel_buffer_dma_dev *pixel_buffer, int stickman_position);
	//@effect: draw 1st frame of sliding stickman

void draw_Stickman_slide_2(alt_up_pixel_buffer_dma_dev *pixel_buffer, int stickman_position);
	//@effect: draw 2nd frame of sliding stickman


