/*
 * gamelogic.c
 *
 *  Created on: 2014-02-04
 *      Author: 1st
 */

#include <stdint.h>
#include <altera_up_avalon_video_pixel_buffer_dma.h>
#include <altera_up_avalon_video_character_buffer_with_dma.h>
#include <io.h>
#include <string.h>
#include "gamelogic.h"
#include "box_sys.h"


void doAction(char input)
{
	////print("I'm in doAction!\n");
	if (!actionLocked) {
		updateStatus(input);
		//print("%i\n", status);
	}
//	if (input == 0x36) {
//		run();
//		unlockAction();
//	}
}

void run(void) {
	status = RUNNING;
	unlockAction();
	////print("I'm Running!\n");
}

void jump(void) {
	status = JUMPING;
	jump_flag=1;
	finishedPlaying = 0;
	////print("I'm Jumping!\n");
}

void slide(void) {
	status = SLIDING;
	slide_flag=1;
	finishedPlaying = 0;
	////print("I'm Sliding!\n");
}

void updateStatus(char input) {
	switch (input) {
		case UP:
			jump();
			lockAction();
			break;
		case DOWN:
			//print("SLIDING!!\n");
			slide();
			lockAction();
			break;
		default:
			break;
	}
}

void lockAction() {
	if (!actionLocked) {
		 actionLocked = 1;
	}
}

void unlockAction() {
		actionLocked = 0;
}

int checkCollision() {
	if (collision_box.TL_x <= boxX[0] && boxX[0] <= collision_box.TR_x && collision_box.TL_y <= boxY[0] && boxY[0] <= collision_box.BL_y) {
		////print("Collision!!\n");
		return 0;
	}
	else if (collision_box.TL_x <= boxX[1] && boxX[1] <= collision_box.TR_x && collision_box.TL_y <= boxY[1] && boxY[1]<= collision_box.BL_y) {
		////print("Collision!!\n");
		return 0;
	}
	else if (collision_box.TL_x <= boxX[2] && boxX[2]<= collision_box.TR_x && collision_box.TL_y <= boxY[2] && boxY[2] <= collision_box.BL_y) {
		////print("Collision!!\n");
		return 0;
	}
	else if (collision_box.TL_x <= boxX[3] && boxX[3] <= collision_box.TR_x && collision_box.TL_y <= boxY[3] && boxY[3] <= collision_box.BL_y) {
		////print("Collision!!\n");
		return 0;
	}
	else if (boxX[0] <= collision_box.TL_x && collision_box.TL_x <= boxX[1] && boxY[0] <= collision_box.TL_y && collision_box.TL_y <= boxY[2]) {
		////print("Collision!!\n");
		return 0;
	}
	else if (boxX[0] <= collision_box.TR_x && collision_box.TR_x <= boxX[1] && boxY[0] <= collision_box.TR_y && collision_box.TR_y <= boxY[2]) {
		////print("Collision!!\n");
		return 0;
	}
	else if (boxX[0] <= collision_box.BL_x && collision_box.BL_x <= boxX[1] &&boxY[0] <= collision_box.BL_y && collision_box.BL_y <= boxY[2]) {
		////print("Collision!!\n");
		return 0;
	}
	else if (boxX[0] <= collision_box.BL_x && collision_box.BL_x <= boxX[1] && boxY[0] <= collision_box.BL_y && collision_box.BL_y <= boxY[2]) {
		////print("Collision!!\n");
		return 0;
	}
	else
		////print("NO collision!!\n");
		return 1;
}

void check_jump(int stickman_speed){
	//@effects:

	if (jump_flag == 1 && drop_flag == 0 && slide_flag == 0){
		// Jumping (moving up)
		stickman_position=stickman_position-stickman_speed;
		update_jumping_player_Box_coords(&collision_box,stickman_speed);
	}
	else if(jump_flag == 1 && drop_flag==1 && slide_flag == 0){
		// Dropping (moving down)
		stickman_position=stickman_position+stickman_speed;
		update_dropping_player_Box_coords(&collision_box,stickman_speed);
	}
}

void check_drop(void){
	//@effects:

	if(stickman_position<-80){
		drop_flag=1;
	}
	else if(stickman_position>=0){
		//stickman_position=0;
		stickman_position=0;
		status=RUNNING;
		drop_flag=0;
		jump_flag=0;		//Need to change this to work with Charles' Jumping status
		update_resetting_player_Box_coords(&collision_box);
		run();
	}
}

void check_slide(int stickman_speed){
	//@effects:

	if(slide_flag==1 && count_slide<=160 && jump_flag == 0){	//jump distance == 160 == 80 + 80. This makes slide distance equal to jump distance.
		count_slide=count_slide+stickman_speed;
		update_sliding_player_Box_coords( &collision_box);
		slide();
	}
	else if(count_slide>160){
		count_slide=0;
		slide_flag=0;
		update_resetting_player_Box_coords(&collision_box);
		run();
	}
}




//int assertEqual (int expected, int actual, int * testCount)
//{
//	*testCount ++;
//	if (expected == actual)
//		//print("%i test passed", testCount);
//	else
//	{
//		//print ("%i test FAILED!!!!!!!", *testCount);
//	}
//}
