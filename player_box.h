/*
 * player_box.h
 *
 *  Created on: 2014-02-01
 *      Author: 1st
 */

#ifndef PLAYER_BOX_H_
#define PLAYER_BOX_H_

typedef struct player_Box_{
	int TL_x;
	int TR_x;
	int BL_x;
	int BR_x;
	int TL_y;
	int TR_y;
	int BL_y;
	int BR_y;
	//int exist; //for testing purpose
}player_Box;

void get_player_Box_coords_x(player_Box collision_box, int *player_BoxX);
	//@Effects: Copy collision_box.TL_x, collision_box.TR_x, collision_box.BL_x, collision_box.BR_x to an int array.
	// 			Returns an array that contains all 4 variables in the same order.

void get_player_Box_coords_y(player_Box collision_box, int *player_BoxY);
	//@Effects: Copy collision_box.TL_y, collision_box.TR_y, collision_box.BL_y, collision_box.BR_y to an int array.
	// 			Returns an array that contains all 4 variables in the same order.

void update_sliding_player_Box_coords(player_Box *collision_box);
	//@Effects: Change the coordinates of all 4 points of the player_Box when sliding flag is active.

void update_jumping_player_Box_coords(player_Box *collision_box,int stickman_speed);
	//@Effects: Change the coordinates of all 4 points of the player_Box when jump_flag = 1 && drop_flag == 0.

void update_dropping_player_Box_coords(player_Box *collision_box, int stickman_speed);
	//@Effects: Change the coordinates of all 4 points of the player_Box when jump_flag = 1 && drop_flag == 1.

void update_resetting_player_Box_coords(player_Box *collision_box);
	//@Effects: Change the coordinates of all 4 points of the player_Box when jump_flag = 0 && drop_flag == 0.


#endif /* PLAYER_BOX_H_ */
