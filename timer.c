///*
// * timer.c
// *
// *  Created on: 2014-01-28
// *      Author: Henry
// *
// *  - Measures clock ticks
// *  - Measures duration of events
// *  - Executes function at specific times
// *  - Count down
// *
// */
//#include <timer.h>
//#include <altera_avalon_timer_regs.h>
//#include "sys/alt_alarm.h"
//#include "sys/alt_timestamp.h"
//#include <stddef.h>
//#include "io.h"
//#include "system.h"
//#include "alt_types.h"
//
//void sys_clock_timer(void)
//{
//	int freq;
//	int cycles;
//	float duration;
//	int ticks_start;
//	int ticks_end;
//	int ticks_per_s;
//	int ticks_duration;
//
//	printf("Timers\n");
//
//	printf(" Sys Clock Timer\n");
//	ticks_per_s = alt_ticks_per_second();
//	printf(" Tick Freq: %d\n", ticks_per_s);  //Display system clock timer
//
//	printf(" Recording starting ticks\n");
//	ticks_start = alt_nticks();
//
//	printf(" Sleeping for 5 seconds\n");
//	usleep(5000000);                           //Sleep for 5 seconds
//
//	printf(" Recording ending ticks\n");
//	ticks_end = alt_nticks();
//	ticks_duration = ticks_end - ticks_start;
//	duration = (float) ticks_duration / (float) ticks_per_s;
//	printf(" The program slept for %d ticks (%f seconds)\n\n", ticks_duration, duration);
//
//	printf(" Timestamp Timer\n");
//	freq = alt_timestamp_freq();                //Time a piece of coding as it runs
//	printf(" CPU Freq: %d\n", freq);
//
//	printf(" Resetting Timestamp timer\n");
//	alt_timestamp_start();                        //Reset
//
//	printf(" ...Timing the print of this statement...\n");
//	cycles = alt_timestamp();
//	duration = (float) cycles / (float) freq;
//	printf(" It took %d cycles (%f seconds) to print the statement\n\n", cycles, duration);
//
//}
//
//void clk_ticks(int duration3){
//	float elapse, since;
//	elapse = alt_ticks_per_second();
//	since = alt_nticks();
//	printf("per second and total ticks: %f %f", elapse, since);
//	while(alt_nticks() <= duration3 * 1000){
//		printf("Time since program started: %f ticks\n", alt_nticks());
//	}
//
//	printf("%i seconds has passed! \n", duration3);
//	/* alt_alarm_start(): int alt_alarm_start (alt_alarm* alarm,
//					alt_u32 nticks,
//					alt_u32 (*callback) (void* context),
//					void* context);  */
//
//	/*
//	* The callback function.
//	*/
//
//}
//
//void timestamp(int duration)
//{
//
//	unsigned int start_time = 0;
//	unsigned int end_time = 0;
//
//	alt_timestamp_start();
//	printf("Recording starting time: %u\n", start_time);
//	start_time = (unsigned int) alt_timestamp();                      //Insert time stamp, marking the start
//
//	while(alt_timestamp()<= duration * 10004809){
//		printf("Time stamp now: %u clock ticks\n", alt_timestamp());
//		}
//	printf("%i seconds has passed! \n", duration);
//}
//
//
//void hard_timer(int duration2){
//	//@ requires:	duration in seconds
//	//@ effects:	generate a hardware interrupt
//	int timer_period;
//	int status;
//	int done;
//
//	printf(" Hardware-Only Timer\n");
//	printf(" Setting timer period to %i seconds.\n", duration2);              //x seconds count down
//
//	timer_period = 5 * 50000000;                                  //Setting interval = 5 seconds
//
//	IOWR_ALTERA_AVALON_TIMER_PERIODL(HARDWARE_TIMER_BASE, timer_period & 0xFFFF);          //Use IO command to timer address 0x0004040
//	IOWR_ALTERA_AVALON_TIMER_PERIODH(HARDWARE_TIMER_BASE, (timer_period >> 16)&0xffff);
//
//	printf(" Stopping Timer\n");
//	status = IORD_ALTERA_AVALON_TIMER_STATUS(HARDWARE_TIMER_BASE);
//	printf("Status: 0x%x", IORD_ALTERA_AVALON_TIMER_STATUS(HARDWARE_TIMER_BASE));
//	if (status & 0x2) {
//		IOWR_ALTERA_AVALON_TIMER_CONTROL(HARDWARE_TIMER_BASE, 1 << 3);
//	}
//	IOWR_ALTERA_AVALON_TIMER_STATUS(HARDWARE_TIMER_BASE,0);
//	printf("Status: 0x%x", IORD_ALTERA_AVALON_TIMER_STATUS(HARDWARE_TIMER_BASE));
//
//	printf(" Starting Timer\n");
//	IOWR_ALTERA_AVALON_TIMER_CONTROL(HARDWARE_TIMER_BASE, 1 << 2);
//	printf("Status: 0x%x", IORD_ALTERA_AVALON_TIMER_STATUS(HARDWARE_TIMER_BASE));
//	printf(" Waiting for timer to expire...\n");
//	done = 0;
//
//	while (!(0x1&IORD_ALTERA_AVALON_TIMER_STATUS(HARDWARE_TIMER_BASE))) {
//	}
//	printf("Status: 0x%x", IORD_ALTERA_AVALON_TIMER_STATUS(HARDWARE_TIMER_BASE));
//	printf(" %u seconds timer is done\n", timer_period);
//}
//

