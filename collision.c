/*
 * collision.c
 *
 *  Created on: 2014-02-04
 *      Author: 1st
 */

#include <stdint.h>
#include <altera_up_avalon_video_pixel_buffer_dma.h>
#include <altera_up_avalon_video_character_buffer_with_dma.h>
#include <io.h>
#include <string.h>
#include "gamelogic.h"
#include "box_sys.h"
#include "collision.h"


void detectCollision(alt_up_pixel_buffer_dma_dev *pixel_buffer){

	last_box_x=get_last_box_coordinate(boxes,5);
	last_box_lane=get_last_box_lane(boxes,5);
	get_all_four_x_coordinates(boxX,4,last_box_x);
	get_all_four_y_coordinates(boxY,4,last_box_lane);
	////print("\n\n Last Box: %d\n\n",last_box_x);
	int g;
	////print("boxX:[");
	for(g=0;g<4;g++){
	//	//print("%d, ", boxX[g]);
	}
	////print("]\n");

	int h;
	////print("boxY:[");
	for(h=0;h<4;h++){
	//	//print("%d, ", boxY[h]);
	}
	////print("]\n");

	if(checkCollision() == 0){
		//alt_up_pixel_buffer_dma_draw_line(pixel_buffer, 0, 60, 320, 60, 0xF800, 1);
		screen = 4;
	}

}

void draw_collision_box(alt_up_pixel_buffer_dma_dev *pixel_buffer){

	alt_up_pixel_buffer_dma_draw_line(pixel_buffer, collision_box.TL_x, collision_box.TL_y, collision_box.TR_x, collision_box.TR_y, 0xF3FF, 1);
	alt_up_pixel_buffer_dma_draw_line(pixel_buffer, collision_box.TL_x, collision_box.TL_y, collision_box.BL_x, collision_box.BL_y, 0xF3FF, 1);
	alt_up_pixel_buffer_dma_draw_line(pixel_buffer, collision_box.TR_x, collision_box.TR_y, collision_box.BR_x, collision_box.BR_y, 0xF3FF, 1);
	alt_up_pixel_buffer_dma_draw_line(pixel_buffer, collision_box.BL_x, collision_box.BL_y, collision_box.BR_x, collision_box.BR_y, 0xF3FF, 1);

}


