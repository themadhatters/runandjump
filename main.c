/*
 * Main file for Run And Jump
 * Created by EECE 381 - Group 18 - The Madhatters
 *
 *
 */

/*********** Including standard C libraries ***********/
#include <stdio.h>
#include <unistd.h>
#include <system.h>
#include <stddef.h>
#include <io.h>

/*********** Including Altera hardware libraries ***********/
#include <altera_up_avalon_character_lcd.h>
#include <altera_up_avalon_video_pixel_buffer_dma.h>
#include <altera_up_avalon_video_character_buffer_with_dma.h>
#include <altera_up_avalon_ps2.h>		//ps2 keyboard
#include <altera_up_ps2_keyboard.h>		//ps2 keyboard (need both)
#include <sys/alt_alarm.h>
#include <sys/alt_timestamp.h>
#include <alt_types.h>
#include <Altera_UP_SD_Card_Avalon_Interface.h>
#include <altera_up_avalon_audio.h>
#include <altera_up_avalon_audio_and_video_config.h>

/*********** Including other header files ***********/
#include "controls.h"
#include "background.h"
#include "graph_init.h"
#include "box_sys.h"
#include "stickman.h"
#include "timer.h"
#include "player_box.h"
#include "gamelogic.h"
#include "collision.h"
#include "sd_card.h"
#include "mainMenu.h"
#include "score.h"
#include "music.h"


/*********** Global Variables ***********/
// keyboard variables
char ascii_keyboard;

// gamelogic variables
int jump_flag=0;
int drop_flag=0;
int slide_flag=0;
int prevTime=0;
int status;
int stickman_position=0;	//A counter to change the stickman based on the keyboard input
int count_slide=0;

// Level System
int level=1;
int count_level=0;
int stickman_speed=30;
int box_speed=30;

// Score System
unsigned int score=0;
unsigned char displayed_score[10]; //EDIT

// box_sys variables
int x_coordinate_of_boxes[5]={0};
int boxes_lanes[5]={0,0,0,0,0};

// collision detection variables
int last_box_x;
int last_box_lane;
box boxes[5];

// player_Box variables
player_Box collision_box;	//Starting position of Collision Box TL(30, 100) TR(60, 100) BL(30, 200) BR(60, 200)
int boxX[4] = {0};
int boxY[4] = {0};
int playerBoxX[4]={0, 0, 0, 0};
int playerBoxY[4]={0, 0, 0, 0};

// pixel buffer variables
unsigned int pixel_buffer_addr1 = PIXEL_BUFFER_BASE;
unsigned int pixel_buffer_addr2 = PIXEL_BUFFER_BASE + (512 * 240 * 2);

// stickman aninmation variables
int stickman_flag = 0;		//A flag to change stickman animation

// Screen variable that chooses game screen, menu, and death screen
int screen = 0;

// SD Card variables
unsigned char readout [1024];

// Music variables
int alt_irq_register(alt_u32 id, void* context, void (*isr)(void*, alt_u32));
unsigned int sound_data [FILE_SIZE];
unsigned int* sound_buff;
unsigned int byte_data[2];
unsigned int sound_data_counter = 0; 	//check if reach end of segment
unsigned int jump_counter = 0; 	//check if reach end of segment
unsigned int slide_counter = 0; 	//check if reach end of segment
unsigned int hit_counter = 0; 	//check if reach end of segment
unsigned int end_file_counter = 0;		//check if reach end of music
unsigned int end_of_segment = 0;
unsigned int end_of_file = 0;
int header_bit = 0;
short int music_handle;
unsigned int jump_sound [JUMP_SIZE];
unsigned int slide_sound [SLIDE_SIZE];
unsigned int hit_sound [HIT_SIZE];
int finishedPlaying = 1;



/*********** Main Program While Loop ***********/
int main(){

	/* Declaring and Initializing Box system */
	init_boxes(boxes,5);
	create_new_box(boxes,5);

	/* Declaring Timer Variables */
	float duration;
	int ticks_start;
	int ticks_end;
	int ticks_per_s;
	int ticks_duration;
	int timer_period;

	/* Timer Caliberation*/
	//print(" Sys Clock Timer\n");
	ticks_per_s = alt_ticks_per_second();
	//print(" Tick Freq: %d\n", ticks_per_s);
	//print(" Recording starting ticks\n");
	ticks_start = alt_nticks();
	//print(" Sleeping for 0.1 seconds\n");
	usleep(100000);
	//print(" Recording ending ticks\n");
	ticks_end = alt_nticks();
	ticks_duration = ticks_end - ticks_start;
	duration = (float) ticks_duration / (float) ticks_per_s;
	//print(" The program slept for %d ticks (%f seconds)\n\n", ticks_duration,	duration);

	/* Declaring variables for user input*/
	int pressed_and_released=0;
	alt_u8 temp_buf_Keyboard;
	KB_CODE_TYPE decode_mode;

	/* Declaring collision box coordinates */
	collision_box.TL_x = 30;
	collision_box.TR_x = 60;
	collision_box.BL_x = 30;
	collision_box.BR_x = 60;
	collision_box.TL_y = 100;
	collision_box.TR_y = 100;
	collision_box.BL_y = 190;
	collision_box.BR_y = 190;

	/* Initialize hardware */
	alt_up_character_lcd_dev * char_lcd_dev;	// LCD pointer
	alt_up_pixel_buffer_dma_dev* pixel_buffer;	// Pixel buffer pointer
	alt_up_ps2_dev* keyboard_device;          	// Keyboard Pointer
	alt_up_char_buffer_dev *char_buffer;
	alt_up_sd_card_dev *SD_CARD = NULL;
	alt_up_audio_dev *audio_dev;

	/* initialize score */
	char_buffer = alt_up_char_buffer_open_dev("/dev/char_drawer");
	alt_up_char_buffer_init(char_buffer);

	/* Initialize SD card */
	SD_CARD = sd_card_init(SD_CARD);

	/* Initialize keyboard  */
	keyboard_device = init_keyboard(keyboard_device);
	int keyEnabled = 1;

	/* Initialize graphics */
	pixel_buffer = graph_init(pixel_buffer);
	//print("Graphics Unit Initialized \n");

	/* Clearing Char buffer */
	alt_up_char_buffer_clear(char_buffer);

	/* Initialize music */
	audio_dev = av_config_setup();
	audio_irq_init(audio_dev);
//	load_music_header();
//	load_music_body();
	load_sound();
	load_jump_body();
	load_slide_body();
	load_hit_body();
	play_sound(audio_dev);

	// Load jump, slide, hit sound effects


	/* Main Program While Loop Here */
	while(1){

		/*************** Starting Timer ****************/
		ticks_start = alt_nticks();

		/*************** Level System ****************/
		if(count_level==320){
			level++;
			box_speed=box_speed+4;
			stickman_speed=box_speed;
			count_level=0;

			if(box_speed>50){
				box_speed=50;
				stickman_speed=box_speed;
			}
		}

		/*************** Main Menu ****************/
		if (screen == 0) {
			mainMenu(pixel_buffer);
			//			alt_up_char_buffer_clear(char_buffer);
			//			score=0;
			//			level=0;
			box_speed=15;
			stickman_speed=15;
			init_boxes(boxes,5);
			create_new_box(boxes,5);

			play_sound(audio_dev);
		}

		/*************** Load Music if reach the end of segment *****************/
//		if (end_of_segment == 1){
//			end_of_segment = 0;
//			load_music_body();
//			printf("Reached end of segment! \n");
//		}
//
//		if (end_of_file == 1){
//			end_of_file = 0;
//			close_music();
//			load_music_header();
//			load_music_body();
//			printf("Reached end of file! \n");
//		}

		/**************** Clearing Background Buffer *****************/
		alt_up_pixel_buffer_dma_clear_screen(pixel_buffer, 1);

		/**************** Drawing background ******************/
		drawBackground(pixel_buffer);

		/**************** Drawing score ******************/
		itoa(score,displayed_score);
		alt_up_char_buffer_string(char_buffer, "Score: ", 2, 2);
		alt_up_char_buffer_string(char_buffer, displayed_score, 2, 3);

		/**************** Checking for User Input ****************/
		if(decode_scancode(keyboard_device,&decode_mode,&temp_buf_Keyboard,&ascii_keyboard)==0){
			if (keyEnabled == 1) {
				//Up: (char)8
				//Down: (char)2
				// makecode: UP = 0x75, DOWN = 0x72
				doAction(ascii_keyboard);
				keyEnabled = 0;
			}
			if (ascii_keyboard==0x00) {
				keyEnabled = 1;
			}
			//print("%i \t %i %x\n", keyEnabled, screen, ascii_keyboard);
		}

		/************* Jumping/Sliding Game Logic **************/
		check_jump(stickman_speed);	/* [TO-DO] Add sound to Stickman action */
		check_drop();
		check_slide(stickman_speed);

		/**************** Collision Detection****************/




		/* Printing Collision Box Coordinates and Stickman Position for Debugging */
		//print("TL_x: %i, TR_x: %i, BL_x: %i, BR_x: %i \n", collision_box.TL_x, collision_box.TR_x, collision_box.BL_x, collision_box.BR_x);
		//print("TL_y: %i, TR_y: %i, BL_y: %i, BR_y: %i \n", collision_box.TL_y, collision_box.TR_y, collision_box.BL_y, collision_box.BR_y);
		//print("stickman_position: %i \n", stickman_position);

		/* Draw collision box on screen */
		//draw_collision_box(pixel_buffer);

		/* Checking for Collisions here */
		detectCollision(pixel_buffer);

		/**************** Checking for deathscreen ****************/
		if (screen == 4) {
			finishedPlaying = 0;
			/* Stopping the music */
			alt_up_audio_disable_write_interrupt(audio_dev);

			/* Writing High Score */
			printf("Converting score from unsigned int to unsigned char. \n");
			itoa(score, displayed_score);

			read_high_score(readout);
			printf("Readout: %s\n", readout);

			printf("Current display score: %d \n", score);
			int highscore = atoi(readout);
			if (score > highscore)
				write_high_score(displayed_score, 10);

			/* Resetting the score */
			deathScreen(pixel_buffer);
			score = 0;
			alt_up_char_buffer_clear(char_buffer);
			level=1;
			jump_flag=0;
			slide_flag=0;
			status=RUNNING;
			stickman_position=0;
			count_slide=0;
		}

		/**************** Updating Stickman Animation ****************/
		if(status==RUNNING||jump_flag==1){
			if (stickman_flag == 0){
				draw_Stickman_run_1(pixel_buffer, stickman_position);
			}
			else if(stickman_flag == 1)	{
				draw_Stickman_run_2(pixel_buffer, stickman_position);
			}
		}
		else if(slide_flag==1){
			if(stickman_flag == 0){
				draw_Stickman_slide_1(pixel_buffer, stickman_position);
			}
			else if(stickman_flag == 1){
				draw_Stickman_slide_2(pixel_buffer, stickman_position);
			}
		}

		/**************** Move boxes *****************/
		move_all_boxes_to_the_left(boxes, 5, box_speed);
		get_coordinates_of_all_boxes(boxes,x_coordinate_of_boxes,5,5);
		get_lanes(boxes,boxes_lanes,5,5);
		draw_boxes(pixel_buffer);

		/*************** Generating Random Boxes Every 20 Pixels ***************/
		int first_box=get_first_box_coordinate(boxes,5);//the variable stores the coordinate of the box closest to the right edge of the screen.
		if(first_box<100)
			create_new_box(boxes,5);

		/**************** Swap background and foreground ****************/
		alt_up_pixel_buffer_dma_swap_buffers(pixel_buffer);
		//@EFFECTS:	Swap background and foreground buffers
		while (alt_up_pixel_buffer_dma_check_swap_buffers_status(pixel_buffer));
		//@EFFECTS: 	Wait for the swap to complete

		/**************** stickman_flag for alternating animation  *****************/
		if (stickman_flag == 0)
			stickman_flag =1;
		else
			stickman_flag =0;

		/**************** Delay used for debugging ****************/
		//usleep(100000);

		/**************** Increasing difficulty and Updating the score ****************/
		count_level++;
		score=score+level*10;

		/**************** Waiting for Timer to Expire ***************/
		ticks_end = alt_nticks();
		printf("# ticks per iteration: %d\n", ticks_end - ticks_start);

		/**************** Listing all the files in the SD Card ****************/
		//list_files();
	}
}




